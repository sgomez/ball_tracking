
#include "opencv2/cudev.hpp"
#include "opencv2/opencv_modules.hpp"
#include <ball_tracking/cuda/img_proc.hpp>

using namespace cv;
using namespace cv::cuda;

namespace ball_tracking {
  
  namespace {
    __global__ void quadf_log_reg_kernel(const PtrStepSz<uchar3> src,
        const PtrStepSz<uchar3> bkg, const PtrStepSz<double> weights,
        PtrStepSz<double> dst) {
      const unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
      const unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
      const unsigned int n = threadIdx.x + blockDim.x*threadIdx.y;
      
      __shared__ double w[28];
      if (n<28) w[n] = weights(0,n);
      __syncthreads();

      if (y < src.rows && x < src.cols) {
        double sum = 0.0;
        uchar3 it = src(y,x), it_bkg = bkg(y,x);
        const double lfeat[7] = {it.x/255.0, it.y/255.0, it.z/255.0, 
          it_bkg.x/255.0, it_bkg.y/255.0, it_bkg.z/255.0, 1.0};
        unsigned int k=0;
        for (unsigned int i=0; i<7; i++) {
          for (unsigned int j=i; j<7; j++, k++) {
            sum += w[k] * lfeat[i] * lfeat[j];
          }
        }
        dst(y,x) = sum;
        //dst(y,x) = (lfeat[0] + lfeat[1] + lfeat[2]) / 3.0;
      }
    }
  };
  
  namespace cuda {
    /**
     * Require that src and bkg are of equal dimensions and that weights
     * is a vector of 28 dimensions
     */
    void quadf_log_reg(const GpuMat& src, const GpuMat& bkg,
        const GpuMat& weights, GpuMat& dst, Stream& stream) {
      CV_Assert(src.rows==bkg.rows && src.cols==bkg.cols);
      dim3 block(32,8);
      dim3 grid((src.cols + block.x - 1)/block.x,
          (src.rows + block.y - 1) / block.y);

      dst.create(src.size(), CV_64FC1);
      cudaStream_t s = StreamAccessor::getStream(stream);

      quadf_log_reg_kernel<<<grid,block,0,s>>>(src, bkg, weights, dst);

      if (s == 0)
        cudaDeviceSynchronize();
    }
  };

};
