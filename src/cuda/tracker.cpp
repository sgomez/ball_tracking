
#include <opencv2/opencv.hpp>
#include <memory>
#include <functional>
#include <json.hpp>
#include <ball_tracking/cuda/img_proc.hpp>
#include <ball_tracking/cuda/tracker.hpp>
#include <ball_tracking/utils.hpp>
#include <opencv2/core.hpp>
#include <opencv2/cudaarithm.hpp>

using namespace cv::cuda;
using namespace cv;
using json = nlohmann::json;
using namespace std;

namespace ball_tracking {

  namespace cuda {

    namespace {

      /**
       * Color and background based logistic regression
       */
      class CB_log_reg {
        private:
          GpuMat bkg; //!< Background image
          GpuMat weights; //!< Model weights
          vector<Ptr<Filter>> pre; //!< Filtering functions for the images
        public:
   
          void pre_chain(const GpuMat& _src, GpuMat& _dst, Stream& stream = Stream::Null()) {
            GpuMat tmp = _src;
            for (auto p : pre) {
              p->apply(tmp, tmp, stream);
            }
            _dst = tmp;
          }
   
          void operator()(const GpuMat& _src, GpuMat& dst, Stream& stream = Stream::Null()) {
            GpuMat src;
            pre_chain(_src, src, stream);
            if (bkg.empty()) {
              bkg = src;
            }
            quadf_log_reg(src, bkg, weights, dst, stream);
          }

          CB_log_reg(const json& conf, Stream& stream = Stream::Null()) {
            Mat w = json2cvmat(conf.at("weights"));
            weights.upload(w, stream);
            if (conf.count("gauss_smooth")) {
              auto gs = conf.at("gauss_smooth");
              unsigned int ksize = gs.at("size");
              double sigma = gs.at("sigma");
              pre.push_back(createGaussianFilter(CV_8UC3, CV_8UC3, Size(ksize,ksize), sigma, sigma));
            }
            if (conf.count("background")) {
              Mat host_bkg = imread(conf.at("background"), CV_LOAD_IMAGE_COLOR);
              if (!host_bkg.data) {
                throw std::logic_error("File not found for background image");
              }
              bkg.upload(host_bkg, stream);
              pre_chain(bkg, bkg, stream);
            }
          }
      };

    };

    class BallLogLikelihood::Impl {
      public:
        preproc bll;

        Impl(const json& config) {
          const string& type = config.at("type");
          const json& conf = config.at("conf");
          if (type == "cb_log_reg") {
            bll = CB_log_reg(conf);
          } else {
            throw std::logic_error("Type of ball log likelihood algorithm not recognized");
          }
        }
    };

    BallLogLikelihood::BallLogLikelihood(const nlohmann::json& params) {
      _impl = shared_ptr<Impl>(new Impl(params));
    }

    BallLogLikelihood::BallLogLikelihood() {
      _impl = nullptr;
    }
        
    BallLogLikelihood::~BallLogLikelihood() = default;

    void BallLogLikelihood::operator()(const cv::cuda::GpuMat& src, cv::cuda::GpuMat& dst, cv::cuda::Stream& stream) {
      return _impl->bll(src, dst, stream);
    }

    class Binarizer::Impl {
      public:
        double thresh;
        GpuMat bin_img;

        Impl(const json& conf) {
          if (conf.count("p_thresh")) {
            double p = conf.at("p_thresh");
            thresh = log(p/(1-p)); //logit function
          } else if (conf.count("thresh")) {
            thresh = conf.at(thresh);
          }
        }

        void bin(const cv::cuda::GpuMat& src, cv::cuda::GpuMat& dst, cv::cuda::Stream& stream) {
          cv::cuda::threshold(src, bin_img, thresh, 1.0, THRESH_BINARY_INV, stream);
          dst = GpuMat(bin_img.rows, bin_img.cols, CV_8UC1);
          bin_img.convertTo(dst, CV_8UC1, 250.0, 0.0, stream);
        }
    };

    Binarizer::Binarizer(const nlohmann::json& params) {
      _impl = shared_ptr<Impl>(new Impl(params));
    }

    Binarizer::Binarizer() {
      _impl = nullptr;
    }

    Binarizer::~Binarizer() = default;

    void Binarizer::operator()(const cv::cuda::GpuMat& src, cv::cuda::GpuMat& dst, cv::cuda::Stream& stream) {
      _impl->bin(src, dst, stream);
    }



  };

};

