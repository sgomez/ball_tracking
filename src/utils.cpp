
#include <ball_tracking/utils.hpp>
#include <fstream>

using namespace cv;
using namespace std;
using json = nlohmann::json;

namespace ball_tracking {

  nlohmann::json load_json(const std::string& file_name) {
    ifstream in(file_name);
    json ans;
    in >> ans;
    return ans;
  }

  cv::Mat json2cvmat(const nlohmann::json& m) {
    CV_Assert(m.is_array());
    if (m[0].is_number()) {
      Mat ans;
      for (double elem : m) {
        ans.push_back( elem );
      }
      return ans;
    } else {
      Mat ans;
      for (auto elem : m) {
        Mat c_mat = json2cvmat(elem);
        if (ans.rows == 0) ans = c_mat.t();
        else ans.push_back(c_mat.t());
      }
      return ans;
    }
  }

};
