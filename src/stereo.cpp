
#include <ball_tracking/stereo.hpp>
#include <ball_tracking/utils.hpp>
#include <robotics/utils.hpp>
#include <zmqpp/zmqpp.hpp>
#include <string>
#include <unordered_map>
#include <thread>
#include <chrono>
#include <armadillo>
#include <thread>
#include <camera.hpp>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>

namespace logging = boost::log;
using namespace std;
using namespace std::chrono;
using json = nlohmann::json;
using namespace arma;

namespace ball_tracking {

  class MultiObs2D {
    private:
      unsigned int num; //!< ID of the observation
      long long _time; //!< A consolidated time of the observation
      unsigned int _num_obs;
    public:
      vector<robotics::pt2d> obs2d; //!< List of observations
      vector<bool> is_obs; //!< Weather or not the observation was actually made 

      MultiObs2D(unsigned int num_cams) {
        obs2d = vector<robotics::pt2d>(num_cams);
        is_obs = vector<bool>(num_cams, false);
        _num_obs = 0;
      }

      unsigned int num_obs() const {
        return _num_obs;
      }

      long long time() const {
        return _time;
      }

      void add_obs(unsigned int cam_id, long long time, bool is_obs, const robotics::pt2d& x) {
        if (!this->is_obs[cam_id]) {
          this->is_obs[cam_id] = is_obs;
          _time = time;
          _num_obs++;
        }
        obs2d[cam_id] = x;        
      }
  };

  class Stereo {
    public:
      unordered_map<unsigned int, arma::mat> calib;
      bool robust = false;
      double max_pix_err;

      Stereo(const json& conf) {
        json jcalib = conf.at("calib");
        for (auto elem : jcalib) {
          unsigned int id = elem.at("ID");
          mat val = robotics::json2mat( elem.at("val") );
          calib[id] = val;
        }
        if (conf.count("max_pix_err")) {
          robust = true;
          max_pix_err = conf.at("max_pix_err");
        }
      }

      bool operator()(robotics::pt3d& ans, const MultiObs2D& obs) {
        vector<pair<unsigned int,robotics::pt2d>> all_obs;
        for (unsigned int i=0; i<obs.obs2d.size(); i++) {
          if (obs.is_obs[i]) {
            all_obs.push_back(make_pair(i, obs.obs2d[i]));
          }
        }
        if (all_obs.size()<2) return false;
        if (robust) {
          all_obs = robotics::stereo_max_inlier_set(calib, all_obs, max_pix_err);
          if (all_obs.size() < 2) return false;
        }
        ans = robotics::stereo_vision(calib, all_obs);
        return true;
      }

      unsigned int num_cameras() const {
        return calib.size();
      }
  };

  class StereoServer::Impl {
    public:
      unordered_map<unsigned int, mat> calib_mats;
      unordered_map<unsigned int, MultiObs2D> observations;
      thread srv_thread;
      bool running;
      unsigned int num_cams;

      void start(const json& conf) {
        running = true;
        auto method = [this](const json& conf) -> void {
          this->start_server(conf);
        };
        srv_thread = thread(method, conf);
      }

      void start_server(const json& conf) {
        if (conf.count("log")) camera::set_log_config(conf.at("log"));

        // 1) Create client and server sockets
        zmqpp::context context;
        zmqpp::socket pos_recv(context, zmqpp::socket_type::sub);
        zmqpp::socket pos_send(context, zmqpp::socket_type::pub);
        // 1.1) Subscribe to all position publisher in 2 dimensions (in config)
        for (const string& url : conf.at("obs2d")) {
          BOOST_LOG_TRIVIAL(info) << "Connecting to " << url;
          pos_recv.connect(url);
        }
        pos_recv.set(zmqpp::socket_option::subscribe, "");
        // 1.2) And create a publisher of 3 dimensional positions
        string pos_send_url = conf.at("publisher");
        pos_send.bind(pos_send_url);
        BOOST_LOG_TRIVIAL(info) << "Creating publisher at " << pos_send_url;
        // 1.3) Read additional configuration
        Stereo stereo(conf.at("stereo"));
        num_cams = stereo.num_cameras();
        BOOST_LOG_TRIVIAL(info) << "Loading stereo configuration for " << num_cams << " cameras";
        unsigned int cache_size_warning = 4;
        while (running) {
          //2) Read the next 2 dim position
          zmqpp::message msg;
          pos_recv.receive(msg);
          string body;
          msg >> body;
          BOOST_LOG_TRIVIAL(trace) << "Received message: " << body;
          json jobs = json::parse(body);

          //3) Create a new MultiObs2D or modify an existing one
          unsigned int num = jobs.at("num");
          unsigned int cam_id = jobs.at("cam_id");
          long long time = jobs.at("time");
          robotics::pt2d x; bool is_obs = false;
          if (observations.count(num) == 0) {
            observations.insert({num, MultiObs2D(num_cams)});
          }
          if (!jobs.at("obs").is_null()) {
            is_obs = true;
            json jx = jobs.at("obs");
            x[0] = jx[0]; x[1] = jx[1];
          }
          MultiObs2D& curr = observations.at(num);
          curr.add_obs(cam_id, time, is_obs, x);
          
          //4) If we have all observations, run stereo
          if (curr.num_obs() == num_cams) {
            BOOST_LOG_TRIVIAL(debug) << "Running stereo for num " << num << " with " << curr.num_obs() << " observations";
            for (int i=0; i<curr.num_obs(); i++) {
              if (curr.is_obs[i]) BOOST_LOG_TRIVIAL(trace) << "Obs " << i << ": (" << curr.obs2d[i][0] << ", " << curr.obs2d[i][1] << ")";
              else BOOST_LOG_TRIVIAL(trace) << "Obs " << i << ": null";
            }
            robotics::pt3d pos3d;
            auto t1 = std::chrono::high_resolution_clock::now();
            bool success = stereo(pos3d, curr);
            auto t2 = std::chrono::high_resolution_clock::now();
            double proc_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count();
            zmqpp::message out;
            json out_obs;
            if (success) {
              for (int i=0; i<3; i++) out_obs.push_back(pos3d[i]);
            }
            json out_msg {
              {"num", num},
              {"time", time},
              {"proc_time", proc_time},
              {"obs", out_obs}
            };
            BOOST_LOG_TRIVIAL(trace) << "Sending message: " << out_msg.dump();
            out << out_msg.dump(); 
            pos_send.send(out);
            observations.erase(num);
          }

          // Just for displaying warnings if things do not work properly
          if (observations.size() > cache_size_warning) {
            int nsize = 2*cache_size_warning;
            BOOST_LOG_TRIVIAL(warning) << "Cache size warning growing from " << cache_size_warning << " to " << nsize;
            cache_size_warning = nsize;
          } else if (4*(1+observations.size()) < cache_size_warning) {
            int nsize = cache_size_warning/2;
            BOOST_LOG_TRIVIAL(warning) << "Cache size warning shrinking from " << cache_size_warning << " to " << nsize;
            cache_size_warning = nsize;
          }
        }
      }

      void stop() {
        running = false;
        srv_thread.join();
      }
  };

  StereoServer::StereoServer() {
    _impl = make_shared<Impl>();
  }

  StereoServer::~StereoServer() = default;

  void StereoServer::start(const nlohmann::json& conf) {
    _impl->start(conf);
  }

  void StereoServer::stop() {
    _impl->stop();
  }

};
