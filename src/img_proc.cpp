
#include <ball_tracking/img_proc.hpp>
#include <ball_tracking/utils.hpp>
#include <algorithm>
#include <json.hpp>

using namespace cv;
using namespace std;
using json = nlohmann::json;

namespace ball_tracking {

  cv::Mat map_channel_quad(cv::InputArray _src, cv::InputArray _mean, cv::InputArray _inv_cov) {
    Mat src = _src.getMat(), tmp_mean = _mean.getMat(), tmp_inv_cov = _inv_cov.getMat();
    const unsigned int channels = src.channels();
    CV_Assert(channels == 3);
    double mu[channels];
    double S[channels*channels];
    copy(tmp_mean.begin<double>(), tmp_mean.end<double>(), mu);
    copy(tmp_inv_cov.begin<double>(), tmp_inv_cov.end<double>(), S);

    Mat ans(src.rows, src.cols, CV_64FC1);
    MatIterator_<Vec3b> it=src.begin<Vec3b>(), end=src.end<Vec3b>();
    MatIterator_<double> it_ans = ans.begin<double>();
    for (; it != end; it++, it_ans++) {
      double mdist = 0.0;
      double* sptr = S;
      for (unsigned int i=0; i<channels; i++) {
        double d1 = mu[i] - (*it)[i];
        for (unsigned int j=0; j<channels; j++, sptr++) {
          double d2 = mu[j] - (*it)[j];
          mdist += d1*(*sptr)*d2;
        }
      }
      *it_ans = mdist;
    }
    return ans;
  }

  cv::Mat quadf_log_reg(cv::InputArray _src, cv::InputArray _bkg, cv::InputArray _weights) {
    Mat src = _src.getMat(), bkg = _bkg.getMat(), tmp_weight = _weights.getMat();
    const unsigned int channels = src.channels();
    CV_Assert(channels == 3);
    CV_Assert(src.rows == bkg.rows && src.cols == bkg.cols);
    double w[28];
    copy(tmp_weight.begin<double>(), tmp_weight.end<double>(), w);

    Mat ans(src.rows, src.cols, CV_64FC1);
    MatIterator_<Vec3b> it=src.begin<Vec3b>(), end=src.end<Vec3b>();
    MatIterator_<Vec3b> it_bkg=bkg.begin<Vec3b>();
    MatIterator_<double> it_ans = ans.begin<double>();
    for (; it != end; it++, it_ans++, it_bkg++) {
      double sum = 0.0;
      double lfeat[7] = {(*it)[0]/255.0, (*it)[1]/255.0, (*it)[2]/255.0, 
        (*it_bkg)[0]/255.0, (*it_bkg)[1]/255.0, (*it_bkg)[2]/255.0, 1.0};
      double* wptr = w;
      for (unsigned int i=0; i<7; i++) {
        for (unsigned int j=i; j<7; j++, wptr++) {
          sum += (*wptr) * lfeat[i] * lfeat[j];
        }
      }
      *it_ans = sum;
    }
    return ans;
  }

};
