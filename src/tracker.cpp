
#include <ball_tracking/tracker.hpp>
#include <ball_tracking/img_proc.hpp>
#include <ball_tracking/utils.hpp>
#include <algorithm>
#include <json.hpp>
#include <queue>
#include <unordered_set>
#include <zmqpp/zmqpp.hpp>
#include <rob_proto/frame.pb.h>

#ifdef WITH_CUDA
#include <ball_tracking/cuda/tracker.hpp>
#include <opencv2/core.hpp>
#endif

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>

namespace logging = boost::log;
using namespace cv;
using namespace std;
using json = nlohmann::json;

namespace ball_tracking {

  /*
   * Private functions
   */
  namespace {

    class GaussSmooth {
      private:
        int size;
        double sigma;
      public:
        GaussSmooth(const json& conf) {
          size = conf.at("size");
          sigma = conf.at("sigma");
        }

        cv::Mat operator()(cv::InputArray _src) {
          Mat dst;
          cv::GaussianBlur(_src, dst, Size(size,size), sigma, sigma);
          return dst;
        }
    };

    /**
     * Color and background based logistic regression
     */
    class CB_log_reg {
      private:
        Mat bkg; //!< Background image
        Mat weights; //!< Model weights
        vector<preproc> pre; //!< Preprocessing functions for the images
      public:
 
        Mat pre_chain(cv::InputArray src) {
          Mat tmp = src.getMat();
          for (auto p : pre) {
            tmp = p(tmp);
          }
          return tmp;
        }
 
        cv::Mat operator()(cv::InputArray _src) {
          Mat src = pre_chain(_src);
          if (bkg.empty()) {
            bkg = src;
          }
          return quadf_log_reg(src, bkg, weights);
        }

        CB_log_reg(const json& conf) {
          weights = json2cvmat(conf.at("weights"));
          if (conf.count("gauss_smooth")) {
            pre.push_back(GaussSmooth(conf.at("gauss_smooth")));
          }
          if (conf.count("background")) {
            bkg = pre_chain( imread(conf.at("background"), CV_LOAD_IMAGE_COLOR) );
            if (!bkg.data) {
              throw std::logic_error("File not found for background image");
            }
          }
        }
    };

    /**
     * Network based image log likelihood
     */
    class ZMQ_log_lh {
      private:
        shared_ptr<zmqpp::context> ctx;
        shared_ptr<zmqpp::socket> sk;

      public:

        ZMQ_log_lh(const json& conf) {
          ctx = shared_ptr<zmqpp::context>(new zmqpp::context);
          sk = shared_ptr<zmqpp::socket>(new zmqpp::socket(*ctx,  zmqpp::socket_type::req));
          sk->connect(conf.at("server"));
        }

        cv::Mat operator()(cv::InputArray _src) {
          cv::Mat m = _src.getMat();
          rob_proto::Image img;
          img.set_height(m.rows);
          img.set_width(m.cols);
          img.set_channels(m.channels());
          img.set_data((const char*)m.data, m.rows*m.cols*m.channels());
          string s_img = img.SerializeAsString();
          
          zmqpp::message req,res;
          req << s_img;
          sk->send(req);
          cv::Mat ans (m.rows, m.cols, CV_64FC1);
          sk->receive(res);
          res >> s_img;
          if (s_img.size() != ans.total()*sizeof(double)) {
            BOOST_LOG_TRIVIAL(error) << "Error in ZMQ_log_lh. Obtained response size different than expected";
          } else {
            std::copy(s_img.begin(), s_img.end(), ans.data);
          }
          return ans;
        }
    };

    /**
     * OpenCV Blob detection algorithm
     */
    class CV_blob_detect {
      private:
        double thresh;
        SimpleBlobDetector::Params params;
        Ptr<SimpleBlobDetector> detector;
        Binarizer bin;
      public:
        void set_blob_params(const json& conf) {
          if (conf.count("filterByArea")) {
            params.filterByArea = conf.at("filterByArea");
            params.minArea = conf.at("minArea");
            params.maxArea = conf.at("maxArea");
          }
          if (conf.count("filterByCircularity")) {
            params.filterByCircularity = conf.at("filterByCircularity");
            params.minCircularity = conf.at("minCircularity");
            params.maxCircularity = conf.at("maxCircularity");
          }
          bin = Binarizer(conf.at("binarizer"));
        }

        CV_blob_detect(const json& conf) {
          set_blob_params(conf);
#if CV_MAJOR_VERSION < 3
          detector = Ptr<SimpleBlobDetector>(new SimpleBlobDetector(params));
#else
          detector = SimpleBlobDetector::create(params);
#endif
        }

        vector<KeyPoint> operator()(cv::InputArray llh) {
          vector<KeyPoint> ans;
          Mat bin_img = bin(llh);
          detector->detect(bin_img, ans);
          return ans;
        }
    };

    /**
     * Finds the blob of highest intensity and returns it only if is above
     * a given threshold
     */
    class Max_blob_detect {
      private:
        double high_thresh, low_thresh;
        int maxArea;
        bool ignore_border_blob;
      public:
        void set_blob_params(const json& conf) {
          double phigh = conf.at("high_thresh");
          double plow = conf.at("low_thresh");
          high_thresh = log(phigh/(1-phigh));
          low_thresh = log(plow/(1-plow));
          maxArea = conf.at("maxArea");
          ignore_border_blob = conf.at("ignore_border_blob");
        }

        Max_blob_detect(const json& conf) {
          set_blob_params(conf);
        }

        int h(int rows, const Point& p) {
          return p.x + p.y*rows;
        }

        vector<KeyPoint> operator()(cv::InputArray _llh) {
          Mat llh = _llh.getMat();
          Point maxLoc;
          double maxVal;
          minMaxLoc(llh, 0, &maxVal, 0, &maxLoc);
          vector<KeyPoint> ans;
          const vector<Point>& delta{Point(0,1), Point(0,-1), Point(1,0), Point(-1,0)};
          int rows = llh.rows, cols = llh.cols;
          if (maxVal > high_thresh) {
            deque<Point> q{maxLoc};
            unordered_set<int> s{h(rows,maxLoc)};
            vector<Point> visited{maxLoc};
            for (int i=0; i<maxArea && !q.empty(); i++) {
              Point parent = q.front(); q.pop_front();
              for (const auto& d : delta) {
                Point child = parent + d;
                if (child.x >= 0 && child.x<cols && child.y >= 0 && child.y<rows && 
                    llh.at<double>(child.y, child.x)>low_thresh && s.count(h(rows, child))==0) {
                  q.push_back(child);
                  visited.push_back(child);
                  s.insert(h(rows, child));
                }
              }
            }

            Point2f center(0,0);
            int low_x=cols+1, high_x=-1;
            for (const auto p : visited) {
              if (ignore_border_blob && (p.x==0 || p.x==(cols-1) || p.y==0 || p.y==(rows-1))) {
                return ans; //The detected blob is on the border and should be ignored
              }
              center.x += p.x; center.y += p.y;
              low_x = min(low_x, p.x);
              high_x = max(high_x, p.x);
            }
            ans.push_back( KeyPoint((1.0/visited.size())*center, high_x-low_x+1) );
          }
          return ans;
        }
    };

  };

  class Binarizer::Impl {
    public:
      double thresh;

      Impl(const json& conf) {
        if (conf.count("p_thresh")) {
          double p = conf.at("p_thresh");
          thresh = log(p/(1-p)); //logit function
        } else if (conf.count("thresh")) {
          thresh = conf.at(thresh);
        }
      }

      Mat bin(cv::InputArray src) {
        Mat bin_img;
        threshold(src, bin_img, thresh, 1.0, THRESH_BINARY_INV);
        bin_img.convertTo(bin_img, CV_8U, 255);
        return bin_img;
      }
  };

  Binarizer::Binarizer(const nlohmann::json& params) {
    _impl = shared_ptr<Impl>(new Impl(params));
  }

  Binarizer::Binarizer() {
    _impl = nullptr;
  }

  Binarizer::~Binarizer() = default;

  cv::Mat Binarizer::operator()(cv::InputArray src) {
    return _impl->bin(src);
  }


  class BallLogLikelihood::Impl {
    public:
      preproc bll;

      Impl(const json& config) {
        const string& type = config.at("type");
        const json& conf = config.at("conf");
        if (type == "cb_log_reg") {
          bll = CB_log_reg(conf);
        } else if (type == "zmq_log_lh") { 
          bll = ZMQ_log_lh(conf);
        } else {
          throw std::logic_error("Type of ball log likelihood algorithm not recognized");
        }
      }
  };

  BallLogLikelihood::BallLogLikelihood(const nlohmann::json& params) {
    _impl = shared_ptr<Impl>(new Impl(params));
  }

  BallLogLikelihood::BallLogLikelihood() {
    _impl = nullptr;
  }
      
  BallLogLikelihood::~BallLogLikelihood() = default;

  cv::Mat BallLogLikelihood::operator()(cv::InputArray src) {
    return _impl->bll(src);
  }


  class FindBallBlob::Impl {
    public:
      blob_finder bf;

      Impl(const json& config) {
        const string& type = config.at("type");
        const json& conf = config.at("conf");
        if (type == "cv_blob_detect") {
          bf = CV_blob_detect(conf);
        } else if (type == "max_blob_detect") {
          bf = Max_blob_detect(conf);
        } else {
          throw std::logic_error("Type of the blob finder algorithm not recognized");
        }
      }
  };

  FindBallBlob::FindBallBlob(const nlohmann::json& params) {
    _impl = shared_ptr<Impl>(new Impl(params));
  }

  FindBallBlob::FindBallBlob() {
    _impl = nullptr;
  }

  FindBallBlob::~FindBallBlob() = default;

  std::vector<cv::KeyPoint> FindBallBlob::operator()(cv::InputArray src) {
    return _impl->bf(src);
  }


  /**
   * Implementation of the tracker class
   */
  namespace {

    class CPUTracker {
      private:
        BallLogLikelihood llh;
        FindBallBlob blob_detect;
        unsigned int subsample;
      public:
        CPUTracker(const json& conf) : subsample(0) {
          llh = BallLogLikelihood(conf.at("ball_log_lh"));
          blob_detect = FindBallBlob(conf.at("blob_detection"));
          if (conf.count("subsample")) subsample = conf.at("subsample");
        }

        vector<KeyPoint> operator()(cv::InputArray _img) {
          Mat img = _img.getMat();

          //1) Subsample if necessary
          double fx=1.0, fy=1.0;
          for (unsigned int i=0; i<subsample; ++i) {
            Mat tmp;
            pyrDown(img, tmp);
            fx *= ((double)img.cols)/tmp.cols; 
            fy *= ((double)img.rows)/tmp.rows;
            img = tmp;
          }

          //2) Process image and get the blobs
          Mat llh_img = llh(img);
          auto ans = blob_detect(llh_img);

          //3) Recover the answer to the original problem
          if (subsample != 0) {
            for (auto& kpt : ans) {
              kpt.pt.x *= fx; 
              kpt.pt.y *= fy;
              kpt.size *= (fx + fy)/2.0;
            }
          }
          return ans;
        }
    };

#ifdef WITH_CUDA

    using namespace cv::cuda;

    class GPUTracker {
      private:
        ball_tracking::cuda::BallLogLikelihood llh; //!< Fully implemented in GPU
        FindBallBlob blob_detect; //!< For the moment only implemented in CPU
        cv::cuda::Stream stream;
        unsigned int subsample;
      public:
        GPUTracker(const json& conf) : subsample(0) {
          BOOST_LOG_TRIVIAL(debug) << "Creating a log-likelihood object in the GPU";
          llh = ball_tracking::cuda::BallLogLikelihood(conf.at("ball_log_lh"));
          BOOST_LOG_TRIVIAL(debug) << "GPU log-likelihood creation was successful. Now creating CPU blob";
          blob_detect = FindBallBlob(conf.at("blob_detection"));
          if (conf.count("subsample")) subsample = conf.at("subsample");
        }

        vector<KeyPoint> operator()(cv::InputArray _img) {
          Mat img = _img.getMat();
          //1) Subsample if necessary (in CPU)
          double fx=1.0, fy=1.0;
          for (unsigned int i=0; i<subsample; ++i) {
            Mat tmp;
            cv::pyrDown(img, tmp);
            fx *= ((double)img.cols)/tmp.cols; 
            fy *= ((double)img.rows)/tmp.rows;
            img = tmp;
          }

          //2) Process image (in GPU) and get blobs (in CPU)
          GpuMat gpu_img;
          GpuMat gpu_llh_img;
          gpu_img.upload(img, stream);
          llh(gpu_img, gpu_llh_img, stream);

          Mat llh_img;
          gpu_llh_img.download(llh_img, stream);
          stream.waitForCompletion();
          auto ans = blob_detect(llh_img);

          //3) Recover the answer to the original problem
          if (subsample != 0) {
            for (auto& kpt : ans) {
              kpt.pt.x *= fx; 
              kpt.pt.y *= fy;
              kpt.size *= (fx + fy)/2.0;
            }
          }
          return ans;
        }
    };
#else
    class GPUTracker {
      public:
        GPUTracker(const json& conf) {
          BOOST_LOG_TRIVIAL(error) << "Attempting to create a GPU object on a CPU only compiled library";
          throw new std::logic_error("Calling a GPU function on a CPU only compiled tracker");
        }

        vector<KeyPoint> operator()(cv::InputArray _img) {
          throw new std::logic_error("Calling a GPU function on a CPU only compiled tracker");
        }
    };
#endif
  };

  class Tracker::Impl {
    public:
      blob_finder tracker;

      Impl(const json& conf) {
        string type = conf.at("type");
        if (type == "cpu") {
          BOOST_LOG_TRIVIAL(debug) << "Creating a CPU tracker...";
          tracker = CPUTracker(conf.at("conf"));
        } else if (type == "gpu") {
          BOOST_LOG_TRIVIAL(debug) << "Creating a GPU tracker...";
          tracker = GPUTracker(conf.at("conf"));
          BOOST_LOG_TRIVIAL(debug) << "GPU tracker successfully created";
        } else {
          throw std::logic_error("Type of tracker selected in the configuration is not recognized");
        }
      }
  };

  Tracker::Tracker(const nlohmann::json& params) {
    _impl = shared_ptr<Impl>(new Impl(params));
  }

  Tracker::Tracker() {
    _impl = nullptr;
  }

  Tracker::~Tracker() = default;
      
  std::vector<cv::KeyPoint> Tracker::operator()(cv::InputArray src) {
    return _impl->tracker(src);
  }

};
