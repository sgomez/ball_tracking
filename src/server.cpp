
#include <ball_tracking/server.hpp>
#include <ball_tracking/tracker.hpp>
#include <ball_tracking/utils.hpp>
#include <zmqpp/zmqpp.hpp>
#include <string>
#include <unordered_map>
#include <thread>
#include <mutex>
#include <chrono>
#include <camera.hpp>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>

namespace logging = boost::log;
using namespace std;
using namespace std::chrono;
using json = nlohmann::json;
using namespace camera;

namespace ball_tracking {

  class TrackServer::Impl {
    public:
      unordered_map<unsigned int,Tracker> trackers;
      ThreadedListener tlistener;
      unique_ptr<zmqpp::context> context;
      unique_ptr<zmqpp::socket> position_pub;
      CameraSet cams;
      high_resolution_clock::time_point start_time;
      std::mutex sock_mtx;
      bool running;

      Impl() {
        position_pub = nullptr;
      }

      void start_trackers(const json& conf) {
        for (auto tracker : conf) {
          unsigned int ID = tracker.at("ID");
          BOOST_LOG_TRIVIAL(info) << "Starting ball tracker with ID " << ID;
          if (tracker.count("file")) {
            string fname = tracker.at("file");
            BOOST_LOG_TRIVIAL(info) << "Reading configuration for ID " << ID << " in " << fname;
            json tconf = load_json(fname);
            trackers[ID] = Tracker(tconf);
            BOOST_LOG_TRIVIAL(info) << "Tracker with ID " << ID << " started successfully";
          } else {
            throw std::logic_error("Configuration for the tracking object expected but not given");
          }
        }
      }

      void send(zmqpp::message& msg) {
        std::unique_lock<mutex> lock(sock_mtx);
        position_pub->send(msg);
      }

      void start_server(const json& conf) {
        if (conf.count("log")) set_log_config(conf.at("log"));
        start_time = high_resolution_clock::now();
        BOOST_LOG_TRIVIAL(info) << "Tracking server started at time " << start_time.time_since_epoch().count();

        //1) Start the networking sockets
        const json& srv_conf = conf.at("servers");
        context = unique_ptr<zmqpp::context>(new zmqpp::context);
        auto socket_type = zmqpp::socket_type::pub;
        position_pub = unique_ptr<zmqpp::socket>(new zmqpp::socket(*context, socket_type));
        const string& pp_url = srv_conf.at("position_publisher");
        BOOST_LOG_TRIVIAL(info) << "Starting 2D position publisher server in " << pp_url;
        position_pub->bind(pp_url);

        //2) Start the tracking pipeline
        start_trackers(conf.at("trackers"));        

        //3) Start the cameras and call backs
        BOOST_LOG_TRIVIAL(info) << "Starting the cameras...";
        BOOST_LOG_TRIVIAL(info) << "Driver: " << conf.at("cameras").at("type");
        cams = CameraSet(conf.at("cameras"));
        BOOST_LOG_TRIVIAL(info) << "Cameras successfully started";
        for (auto& p: trackers) {
          unsigned int ID = p.first;

          //3.1) Create a call-back
          auto call_back = [ID, this](const Frame& frame) -> void {
            BOOST_LOG_TRIVIAL(debug) << "Obs2D call-back called on camera " << ID << 
              " Frame: {cam_id: " << frame.cam_id << ", num:" << frame.num << 
              ", time: " << frame.time.time_since_epoch().count() << "}";
            Tracker track = this->trackers.at(ID);
            vector<cv::KeyPoint> obs2d = track(frame.img);
            json obs;
            if (obs2d.size() != 0) {
              obs.push_back(obs2d[0].pt.x);
              obs.push_back(obs2d[0].pt.y);
            }
            json jframe {
              {"cam_id", frame.cam_id},
              {"num", frame.num},
              {"time", frame.time.time_since_epoch().count()},
              {"obs", obs}
            };
            BOOST_LOG_TRIVIAL(debug) << "Sending message: " << jframe.dump();
            zmqpp::message msg;
            msg << jframe.dump();
            this->send(msg);
          };

          //3.2) Add the call-back as a new thread
          BOOST_LOG_TRIVIAL(debug) << "Adding a threaded call-back for camera " << ID;
          tlistener.add_listener(ID, call_back);
        }
        BOOST_LOG_TRIVIAL(debug) << "Starting all the camera listeners";
        running = true;
        cams.start(tlistener);
      }

      void stop() {
        if (running) {
          running = false;
          BOOST_LOG_TRIVIAL(info) << "Stopping the position publisher server";
          cams.stop();
          tlistener.stop();
        }
      }

      ~Impl() {
        stop();
      }
  };

  TrackServer::TrackServer() {
    _impl = make_shared<Impl>();
  }
      
  TrackServer::~TrackServer() = default;
      
  void TrackServer::start(const nlohmann::json& conf) {
    _impl->start_server(conf);
  }
      
  void TrackServer::stop() {
    _impl->stop();
  }

};
