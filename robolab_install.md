Install the Robolab software un Ubuntu 18
=========================================

Install some prerequisites
--------------------------

Basic tools from repos:

> sudo apt-get install libarmadillo-dev cmake libnlopt-dev python-dev swig

ZMQ from repos:

> sudo apt-get install libzmqpp-dev python-zmq python3-zmq libzmq3-dev

Boost from repos:

> sudo apt-get install libboost-dev libboost-test-dev libboost-log-dev \
  libboost-program-options-dev

Install protocol buffers
------------------------

Unfortunately, we need to install this one from source. I tested the version 3.6.1
that can be downloaded from:

https://github.com/protocolbuffers/protobuf/releases/tag/v3.6.1

Then I used to install:

./configure --prefix=$HOME/software
make -j10
make install

Install The Network Protocols
-----------------------------

git clone https://gitlab.tuebingen.mpg.de/sgomez/rob_protos.git
cd rob_protos
mkdir build
cd build/
cmake .. -DCMAKE_INSTALL_PREFIX=$HOME/software -DCMAKE_BUILD_TYPE=Release
make -j10
make install
cd ../..


Install RobCPP Libraries
------------------------

Commands:

git clone https://gitlab.tuebingen.mpg.de/sgomez/robcpp.git
cd robcpp
git fetch
git checkout dev

Install lib robotics:

cd lib_robotics/
mkdir build
cd build/
cmake .. -DCMAKE_INSTALL_PREFIX=$HOME/software -DCMAKE_BUILD_TYPE=Release -DPYLIB=Off
make -j10
make install
cd ../..

Before proceeding with the compilation of the rest of the libraries make sure your install path is part of the environment variables you want to have.

Install table_tennis:

cd table_tennis/
mkdir build
cd build/
cmake .. -DCMAKE_INSTALL_PREFIX=$HOME/software -DCMAKE_BUILD_TYPE=Release -DPYLIB=Off
make -j10mke install
cd ../..

Install SLPP:

cd slpp/
mkdir build
cd build/
cmake .. -DCMAKE_INSTALL_PREFIX=$HOME/software -DCMAKE_BUILD_TYPE=Release
make -j10
make install
cd ../..


Installing the Python Package
-----------------------------

Install Pre-requisites:

sudo apt-get install python-opencv python-sphinx python3-tk python-tk


Get the package and install it:

git clone https://gitlab.tuebingen.mpg.de/sgomez/robpy.git
cd robpy
git fetch
git checkout dev
python setup.py develop --user

The last line can be replaced by "python setup.py install" if you don't want to change
the python code. To be able to run all the scripts it is recommended to install all
the SciPy stack, that can be installed with:

sudo apt-get install python-numpy python-scipy python-matplotlib


Installing the Vision System
============================

The following instructions are only necessary for the vision system computer. It doesn't
hurt to install it in any other computer in any case. Note that in order to be able to
run the vision system at the proper speed you need probably a set of GPUs and a CUDA system
installed.

Install the Camera Drivers
--------------------------

This part of the tutorial assumes you have a set of Allied Vision cameras connected via
Ethernet to your computer. You need to download and install the Allied Vision PvAPI drivers
that I particularly downloaded from:

https://www.alliedvision.com/fileadmin/content/software/software/PvAPI/PvAPI_1.28_Linux.tgz

To install it I installed frist the following pre-requisites:


and subsequently proceeded to:

1) Copy the files bin-pc/x64/lib*.so to your library path
2) Paste the executables in bin-pc/x64/ to your */bin/ folder (a path added to your $PATH)
3) Copy the header files inc-pc/*.h to your */include (folder where the compiler
   look for header files)

To test that the installation worked you should be able to open a terminal and type:

> CLIpConfig -l

This command should print a list with the cameras connected to your computer.


Install the Camera C++ Bindings
-------------------------------

I created a small C++ wrapper to be able to use the cameras easily. This wrapper is
necessary to install the ball tracking code.

First install the pre-requisites:

sudo apt-get install libopencv-dev

git clone https://gitlab.tuebingen.mpg.de/sgomez/camera.git
cd camera
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$HOME/software -DCMAKE_BUILD_TYPE=Release
make -j10
make install


Install the Ball Tracking Software
----------------------------------

After the camera C++ bindings are installed, we can proceed to install the ball
tracking software. Make sure CUDA is installed in your system with version at least
9.2.

To use the GPUs, I think you need to compile OpenCV from source. I downloaded
OpenCV 3.4.5. I tried also the version 4.0.1 but didn't work unless the code is
changed.

mkdir build
cd build
cmake -DOPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=$HOME/software -D WITH_CUDA=ON -DBUILD_opencv_cudacodec=OFF ..
make
make install

Then install the ball tracking software itself:

git clone https://gitlab.tuebingen.mpg.de/sgomez/ball_tracking.git
cd ball_tracking
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$HOME/software -DCMAKE_BUILD_TYPE=Release
make -j10
make install


Installing additional Python packages for research
--------------------------------------------------

pip install keras tensorflow-gpu matplotlib sklearn
pip3 install keras tensorflow-gpu matplotlib sklearn


