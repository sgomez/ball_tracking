Ball Tracking Code
==================

This library for ball tracking contains the implementation of the methods presented on this 
[paper](https://www.mdpi.com/2218-6581/8/4/90). Some of the design goals of the library are:

* Support different camera hardware
* Support different algorithms along the pipeline for object detection
* Easy to configure from a configuration file (JSON format)
* Publisher-Subscriber architecture servers for the vision system
* The algorithms can run on a CPU or GPU

### Support for different camera hardware

* The camera support is implemented in a separate library called 
[libcamera](https://gitlab.tuebingen.mpg.de/sgomez/camera).
* At the time of this writing we only implemented the **PvAPI** interface for Prosillica cameras. 
* To support new cameras you only need to implement a class that Inherits from **CamSetDriver**
* Objects from the CamSetDriver class receive the camera set configuration and a callback to be called
when new images are received.

## Algorithm Pipeline

Implemented in "tracker.hpp":

* **BallLogLikelihood**: Receives an image and returns a likelihood image (of each pixel being the ball)
* **Binarizer**: Turns the likelihood image into a binary image (Ball, Not ball)
* **FindBallBlob**: Returns all candidate locations of the ball

**Note**: The algorithms used are implemented both in the CPU and the GPU

### Configuration

How do we select:
* Which algorithms to use on each step of the pipeline
* The model parameters (when using ML)
* The hyperparameters of **Binarizer** and **FindBallBlob**
* If the algorithms should run in the CPU or GPU

We do all that using a configuration file in JSON format. See an example in 
[examples/tracking/cpu_track_conf.json](./examples/tracking/cpu_track_conf.json)

## Publisher - Subscriber

We have two separate important processes that communicate using the Publisher-Subscriber architecture
* [server](./examples/tracking/server.cpp): Broadcasts the 2D pixel location of the ball in all the images to
all its subscribers
* [server3d](./examples/tracking/server3d.cpp): Subscribes to the 2D server and broadcasts the 3D position
in world coordinates of the ball to all its subscribers

Both services use configuration files (JSON). You can see examples in the example folder.

### Client (Subscriber) Example

* The client can be written in any programming language
* Can also be executed in a separate machine as the vision system
* Assuming the server runs in a machine called "Rodau" in the TCP port 7660, see 
the following example written in Python:

```python
import sys
import zmq
import json

url = "tcp://rodau:7660"
context = zmq.Context()
socket = context.socket(zmq.SUB)

print "Connecting to {0}".format(url)
socket.connect(url)

topicfilter = ""
socket.setsockopt(zmq.SUBSCRIBE, topicfilter)

num_obs = 1000
obs = []
ball_in_scene = False
null_warn = 10
while (len(obs) < num_obs):
    #topic = socket.recv()
    body = socket.recv()
    msg = json.loads(body)
    if msg['obs'] is None and null_warn>0:
        print "Observing null"
        null_warn-=1
    if msg['obs'] is not None and not ball_in_scene:
        ball_in_scene = True
        print "First ball observed"
    if ball_in_scene:
        obs.append(msg)
        print msg
```

* The example fills the list **obs** with all the 3D observations returned by the vision system until
1000 are read
* If there is no ball in the scene, the server returns **null**.

## Installation

The first step to install this library is to install the pre-requisits. The easiest way to install these
pre-requisits is to use your OS package manager, but make sure that the versions on your OS package manager
are recent. Below I provide the links to the official mantainers of the required packages, in case you
decide to install these packages from the source (For instance, if your OS package versions are too old).

1. [OpenCV](http://opencv.org/)
2. [Boost](http://www.boost.org/)
3. [CMake](https://cmake.org/)
4. [ZMQ](http://zeromq.org/)

In a modern version of Ubuntu you might be able to get the right version of these packages using:

```
sudo apt-get install libopencv-dev libboost-dev libboost-log-dev libboost-program-options-dev \
  libboost-test-dev cmake libzmqpp-dev
```

However, it is very likely that the software version is not up to date. We use OpenCV 3.2, 
Boost libraries 1.58 and CMake 3.5.1.

After the pre-requisites are installed, download the source of this library, compile and install. You need
CMake to be able to compile and install this library.

To install follow the standard procedure to compile packages with CMake, for instance, in Linux you would 
type:

```
mkdir build
cd build
cmake ..
make
make test #optional (Tests that the compiled library passes the test cases)
make install
```

To do `make install` you probably need root permisions. You can also install the library in your home
folder if you do not have root permisions, by changing the CMake installation prefix.
