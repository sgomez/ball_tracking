#ifndef BALL_TRACKING_TRACKER
#define BALL_TRACKING_TRACKER

#include <opencv2/opencv.hpp>
#include <memory>
#include <functional>
#include <json.hpp>

namespace ball_tracking {

  /**
   * @brief Data type for an Image pre-processing function.
   */
  using preproc = std::function<cv::Mat(cv::InputArray src)>;

  /**
   * @brief Data type for a system that find desired blobs in an image
   */
  using blob_finder = std::function<std::vector<cv::KeyPoint>(cv::InputArray src)>;

  /**
   * @brief Produces a single channel image highlighting the possible position of the ball
   * according to a likelihood function
   */
  class BallLogLikelihood {
    private:
      class Impl;
      std::shared_ptr<Impl> _impl;
    public:
      /**
       * @brief Creates an object with given configuration parameters
       */
      BallLogLikelihood(const nlohmann::json& params);

      /**
       * @brief Creates an empty object. 
       *
       * Using an object created with this default constructor will result 
       * in undefined behaviour.
       */
      BallLogLikelihood();

      ~BallLogLikelihood();

      /**
       * @brief Produces a single channel image representing the log likelihood of each
       * pixel being part of the ball
       *
       * @params[in] src The source image
       * @returns a new single channel image with the same dimensions of the input image,
       * where every pixel contains the log likelihood of the corresponding pixel in the
       * input image corresponding to a table tennis ball
       */
      cv::Mat operator()(cv::InputArray src);
  };

  /**
   * @brief Thresholds an image
   */
  class Binarizer {
    private:
      class Impl;
      std::shared_ptr<Impl> _impl;
    public:
      /**
       * @brief Creates a binarizer with the given configuration
       */
      Binarizer(const nlohmann::json& params);

      /**
       * @brief Creates an empty object
       */
      Binarizer();

      ~Binarizer();

      /**
       * @brief Returns an 8 bit single channel binarized image from a given single channel
       * real valued image. The image contains only 0 or 255 values in each pixel.
       *
       * @param[in] src A single channel image where threshold is to be applied
       * @returns a single 8 bit channel image with values 0 or 255 only.
       */
      cv::Mat operator()(cv::InputArray src);
  };

  /**
   * @brief Find candidate ball positions if any in a likelihood image 
   */
  class FindBallBlob {
    private:
      class Impl;
      std::shared_ptr<Impl> _impl;
    public:
      FindBallBlob(const nlohmann::json& params);
      ~FindBallBlob();

      /**
       * @brief Creates an empty object
       */
      FindBallBlob();

      /**
       * @brief Returns all candidate locations of a ball in a single channel
       * likelihood image.
       *
       * @param[in] src A binary (thresholded) image segmenting the interesting parts
       * @returns a vector of Key Points with the locations and other optional
       * properties of the ball blobs found in the image
       */
      std::vector<cv::KeyPoint> operator()(cv::InputArray src);
  };

  /**
   * @brief Returns the object position if any from the original image
   */
  class Tracker {
    private:
      class Impl;
      std::shared_ptr<Impl> _impl;
    public:
      Tracker(const nlohmann::json& params);
      Tracker();
      ~Tracker();
      std::vector<cv::KeyPoint> operator()(cv::InputArray src);
  };

};

#endif
