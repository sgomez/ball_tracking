#ifndef BALL_TRACKING_CUDA_TRACKER
#define BALL_TRACKING_CUDA_TRACKER

#include <opencv2/opencv.hpp>
#include <memory>
#include <functional>
#include <json.hpp>

namespace ball_tracking {

  namespace cuda {

    /**
     * @brief Data type for an Image pre-processing function.
     */
    using preproc = std::function<void(const cv::cuda::GpuMat& src, cv::cuda::GpuMat& dst, cv::cuda::Stream& stream)>;

    /**
     * @brief Produces a single channel image highlighting the possible position of the ball
     * according to a likelihood function
     */
    class BallLogLikelihood {
      private:
        class Impl;
        std::shared_ptr<Impl> _impl;
      public:
        /**
         * @brief Creates an object with given configuration parameters
         */
        BallLogLikelihood(const nlohmann::json& params);
        ~BallLogLikelihood();

        /**
         * @brief Creates an empty object
         */
        BallLogLikelihood();

        /**
         * @brief Produces a single channel image representing the log likelihood of each
         * pixel being part of the ball
         *
         * @params[in] src The source image
         * @params[out] dst  A new single channel image with the same dimensions of the input image,
         * where every pixel contains the log likelihood of the corresponding pixel in the
         * input image corresponding to a table tennis ball
         * @params[in] stream The stream of execution on the GPU
         */
        void operator()(const cv::cuda::GpuMat& src, cv::cuda::GpuMat& dst, cv::cuda::Stream& stream = cv::cuda::Stream::Null());
    };

    /**
     * @brief Thresholds an image
     */
    class Binarizer {
      private:
        class Impl;
        std::shared_ptr<Impl> _impl;
      public:
        Binarizer(const nlohmann::json& params);
        ~Binarizer();

        /**
         * @brief Creates an empty object
         */
        Binarizer();

        /**
         * @brief Returns an 8 bit single channel binarized image from a given single channel
         * real valued image. The image contains only 0 or 255 values in each pixel.
         *
         * @param[in] src A single channel image where threshold is to be applied
         * @returns a single 8 bit channel image with values 0 or 255 only.
         */
        void operator()(const cv::cuda::GpuMat& src, cv::cuda::GpuMat& dst, cv::cuda::Stream& stream = cv::cuda::Stream::Null());
    };

  };
};

#endif
