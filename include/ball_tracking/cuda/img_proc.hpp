#ifndef BALL_TRACKING_CUDA_IMG_PROC
#define BALL_TRACKING_CUDA_IMG_PROC

#include <opencv2/opencv.hpp>
#include <memory>
#include <functional>

namespace ball_tracking {

  namespace cuda {

    /**
     * @brief Applies a pixel-wise logistic regression with quadratic features to the source
     * image and returns a single-channel image with the log-probabilities in the GPU
     *
     * @param[in] src The source image on the GPU
     * @param[in] bkg A background image (Without the ball) on the GPU
     * @param[in] weights Vector of weights of logistic regression
     * @param[out] dst The destination image in the GPU
     * @param[in] stream The stream of execution in the GPU
     */
    void quadf_log_reg(const cv::cuda::GpuMat& src, const cv::cuda::GpuMat& bkg, 
        const cv::cuda::GpuMat& weights, cv::cuda::GpuMat& dst, 
        cv::cuda::Stream& stream = cv::cuda::Stream::Null());
  };

};

#endif
