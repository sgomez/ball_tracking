#ifndef BALL_TRACKING_IMG_PROC
#define BALL_TRACKING_IMG_PROC

#include <opencv2/opencv.hpp>
#include <memory>
#include <functional>

namespace ball_tracking {

  /**
   * @brief Applies a vector quadratic function \f[f(x) = (x-\mu)^T S (x-\mu)\f] to every pixel
   * of the given multi-channel image and returns a new single-channel image with the results
   *
   * @param[in] src The source image to transform with the quadratic function
   * @param[in] mean A vector with dimensionality equal to the number of channels representing
   * the parameter \f$\mu\f$ of the quadratic function
   * @param[in] S A square matrix with dimensionality equal to the number of channels
   *
   * @returns a new image where every pixel corresponds to the quadratic function applyied to
   * the corresponding pixel in src.
   */
  cv::Mat map_channel_quad(cv::InputArray src, cv::InputArray mean, cv::InputArray S);

  /**
   * @brief Applies a pixel-wise logistic regression with quadratic features to the source
   * image and returns a single-channel image with the log-probabilities
   *
   * @param[in] src The source image
   * @param[in] bkg A background image (Without the ball)
   * @param[in] weights Vector of weights of logistic regression
   *
   * @returns a new image where every pixel corresponds to the log-probability of the
   * pixel being a ball according to the trained classifier 
   */
  cv::Mat quadf_log_reg(cv::InputArray src, cv::InputArray bkg, cv::InputArray weights);

};

#endif
