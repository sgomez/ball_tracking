#ifndef BALL_TRACKING_SERVER
#define BALL_TRACKING_SERVER

#include <memory>
#include <json.hpp>

namespace ball_tracking {

  class TrackServer {
    private:
      class Impl;
      std::shared_ptr<Impl> _impl;
    public:
      TrackServer();
      ~TrackServer();
      void start(const nlohmann::json& conf);
      void stop();
  };
};

#endif
