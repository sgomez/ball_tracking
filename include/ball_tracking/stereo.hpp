#ifndef BALL_TRACKING_STEREO
#define BALL_TRACKING_STEREO

#include <memory>
#include <json.hpp>

namespace ball_tracking {

  class StereoServer {
    private:
      class Impl;
      std::shared_ptr<Impl> _impl;
    public:
      StereoServer();
      ~StereoServer();

      void start(const nlohmann::json& conf);
      void stop();
  };
};

#endif
