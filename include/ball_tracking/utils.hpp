#ifndef BALL_TRACKING_UTILS
#define BALL_TRACKING_UTILS

#include <opencv2/opencv.hpp>
#include <json.hpp>
#include <string>
#include <vector>
#include <iostream>

namespace ball_tracking {

  /**
   * @brief Converts a matrix represented in JSON to a OpenCV matrix
   *
   * @param[in] m The matrix in JSON format
   *
   * @returns a OpenCV matrix 
   */
  cv::Mat json2cvmat(const nlohmann::json& m);

  /**
   * @brief Loads a JSON file into a JSON object
   *
   * @param[in] file_name Path to the file to load
   *
   * @returns a JSON object
   */
  nlohmann::json load_json(const std::string& file_name);

};

#endif
