
#include <ball_tracking/img_proc.hpp>
#include <ball_tracking/utils.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core.hpp>
#include <iostream>
#include <boost/program_options.hpp>
#include <json.hpp>
#include <chrono>

#define USE_GPU

#ifdef USE_GPU
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <ball_tracking/cuda/img_proc.hpp>

using namespace cv::cuda;
#endif

using namespace cv;
using namespace std;
using namespace ball_tracking;
using json = nlohmann::json;

int main(int argc, char** argv) {
  try {
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()
      ("help", "Produce help message")
      ("input,i", po::value<string>(), "path to the input image")
      ("conf,c", po::value<string>(), "path to the JSON configuration")
      ("output,o", po::value<string>(), "path to the output image");
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
      cout << desc << endl;
      return 0;
    }
    if (!vm.count("input")) {
      cerr << "Error: You should provide the image to process" << endl;
      return 1;
    }

    json jobj = load_json(vm["conf"].as<string>());
    Mat model_coef = json2cvmat(jobj.at("model_coefs"));

    Mat back = imread(jobj.at("background"), CV_LOAD_IMAGE_COLOR);
    if(! back.data )   // Check for invalid input background
    {
      cout <<  "Could not open or find the backgroundimage" << std::endl ;
      return -1;
    }
    string fname = vm["input"].as<string>();
    Mat img = imread(fname, CV_LOAD_IMAGE_COLOR);

    /*imshow("Color", back);
    waitKey(0);*/ 
    //Calculate the data with coefs
#ifdef USE_GPU
    cout << "Using the GPU for computation" << endl;
    //1) Load images and weights to GPU
    GpuMat gpu_img(img), gpu_bck(back), gpu_w(model_coef);
    GpuMat gpu_result;
    //2) Do the computation
    auto start_t = std::chrono::steady_clock::now();
    ball_tracking::cuda::quadf_log_reg(gpu_img, gpu_bck, gpu_w, gpu_result);
    auto end_t = std::chrono::steady_clock::now();
    //3) Load back to CPU
    Mat im_proc; 
    gpu_result.download(im_proc);
#else
    cout << "Using the CPU for computation" << endl;
    auto start_t = std::chrono::steady_clock::now();
    Mat im_proc = quadf_log_reg(img, back, model_coef);
    auto end_t = std::chrono::steady_clock::now();
#endif
    std::cout << "Computation took "
              << std::chrono::duration_cast<std::chrono::milliseconds>(end_t - start_t).count() << 
              " milliseconds" << endl;
    //4) Turn it into a probability image for easier interpretation
    for (auto it=im_proc.begin<double>(); it != im_proc.end<double>(); ++it) {
      *it = 1.0 / (1.0 + exp(-*it));
    }
    imshow("Likelihood", im_proc);
    waitKey(0);

    /*cout << "pix data:" << im_proc.at<double>(340, 290) << endl 
      << im_proc.at<double>(342, 291) << endl 
      << im_proc.at<double>(290, 340) << endl
      << im_proc.at<double>(2, 2)   << endl;*/
    //5) Binarize
    for (auto it=im_proc.begin<double>(); it != im_proc.end<double>(); ++it) {
      *it = (*it > 0.95) ? 255 : 0;
    }
    imshow("Color", im_proc);
    waitKey(0); 

    //exp(-0.5*im_proc, im_proc);
    //im_proc = 255*im_proc;
    //imshow("Processed", im_proc);    
    //waitKey(0);
    if (vm.count("output")) {
      imwrite(vm["output"].as<string>(), im_proc);
    }
    return 0;
  } catch (std::exception& ex) {
    cerr << "Exception: " << ex.what() << endl;
    return 1;
  }
}
