
#include <ball_tracking/img_proc.hpp>
#include <ball_tracking/utils.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <boost/program_options.hpp>
#include <json.hpp>

using namespace cv;
using namespace std;
using namespace ball_tracking;
using json = nlohmann::json;

int main(int argc, char** argv) {
  try {
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()
      ("help", "Produce help message")
      ("input,i", po::value<string>(), "path to the input image")
      ("conf,c", po::value<string>(), "path to the JSON configuration")
      ("output,o", po::value<string>(), "path to the output image");
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
      cout << desc << endl;
      return 0;
    }
    if (!vm.count("input")) {
      cerr << "Error: You should provide the image to process" << endl;
      return 1;
    }

    json jobj = load_json(vm["conf"].as<string>());
    Mat mean = json2cvmat(jobj.at("mean"));
    Mat inv_cov = json2cvmat(jobj.at("inv_cov"));

    string fname = vm["input"].as<string>();
    Mat img = imread(fname, CV_LOAD_IMAGE_COLOR);
    //imshow("Color", img);
    Mat im_proc = map_channel_quad(img, mean, inv_cov);
    exp(-0.5*im_proc, im_proc);
    im_proc = 255*im_proc;
    //imshow("Processed", im_proc);    
    //waitKey(0);
    if (vm.count("output")) {
      imwrite(vm["output"].as<string>(), im_proc);
    }
    return 0;
  } catch (std::exception& ex) {
    cerr << "Exception: " << ex.what() << endl;
    return 1;
  }
}
