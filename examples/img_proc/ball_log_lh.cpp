
#include <ball_tracking/tracker.hpp>
#include <ball_tracking/utils.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <boost/program_options.hpp>
#include <json.hpp>
#include <chrono>

using namespace cv;
using namespace std;
using namespace ball_tracking;
using json = nlohmann::json;

int main(int argc, char** argv) {
  try {
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()
      ("help", "Produce help message")
      ("input,i", po::value<string>(), "path to the input image")
      ("conf,c", po::value<string>(), "path to the JSON configuration");
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
      cout << desc << endl;
      return 0;
    }
    if (!vm.count("input")) {
      cerr << "Error: You should provide the video to process" << endl;
      return 1;
    }

    json jobj = load_json(vm["conf"].as<string>());
    BallLogLikelihood bll(jobj.at("ball_log_lh"));
    Binarizer bin(jobj.at("binarizer"));

    string fname = vm["input"].as<string>();
    Mat img = imread(fname, CV_LOAD_IMAGE_COLOR);

    imshow("Color", img);
    waitKey(0); 
    //Calculate the data with coefs
    auto start_t = std::chrono::steady_clock::now();
    Mat im_proc = bll(img);
    auto end_t = std::chrono::steady_clock::now();
    std::cout << "Computation of log likelihoods took "
              << std::chrono::duration_cast<std::chrono::milliseconds>(end_t - start_t).count() << 
              " milliseconds" << endl;
    Mat log_lh_norm;
    normalize(im_proc, log_lh_norm, 0, 1, NORM_MINMAX);
    imshow("Log likelihood equalized", log_lh_norm);
    waitKey(0);

    //Probabilities
    Mat t1, t2;
    t1 = -im_proc;
    exp(t1, t1);
    t2 = 1.0/(1.0 + t1);
    imshow("Probabilities", t2);
    waitKey(0);

    Mat bin_img;
    start_t = std::chrono::steady_clock::now();
    bin_img = bin(im_proc);
    end_t = std::chrono::steady_clock::now();
    std::cout << "Binarization took "
              << std::chrono::duration_cast<std::chrono::milliseconds>(end_t - start_t).count() << 
              " milliseconds" << endl;
    imshow("Binarized", bin_img);
    waitKey(0); 

    if (vm.count("output")) {
      imwrite(vm["output"].as<string>(), im_proc);
    }
    return 0;
  } catch (std::exception& ex) {
    cerr << "Exception: " << ex.what() << endl;
    return 1;
  }
}
