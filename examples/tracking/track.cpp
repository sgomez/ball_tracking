
#include <ball_tracking/tracker.hpp>
#include <ball_tracking/utils.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <boost/program_options.hpp>
#include <json.hpp>
#include <chrono>
#include <cmath>

using namespace cv;
using namespace std;
using namespace ball_tracking;
using json = nlohmann::json;

struct TimeStats {
  double mean;
  double std;
};

TimeStats comp_stats(const vector<double>& time) {
  double sum_1 = 0.0, sum_sq = 0.0;
  for (double t : time) {
    sum_1 += t;
    sum_sq += t*t;
  }
  int N = time.size();
  double mu = sum_1 / N;
  double var = (sum_sq/N) - mu*mu;
  return {mu, sqrt(var)};
}

ostream& operator<<(ostream& a, const TimeStats& st) {
  a << "{mean=" << st.mean << ", std=" << st.std << "}";
  return a;
}

int main(int argc, char** argv) {
  try {
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()
      ("help", "Produce help message")
      ("input,i", po::value<string>(), "path to the input video")
      ("output,o", po::value<string>(), "path to the output video")
      ("conf,c", po::value<string>(), "path to the JSON configuration");
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
      cout << desc << endl;
      return 0;
    }
    if (!vm.count("input")) {
      cerr << "Error: You should provide the video to process" << endl;
      return 1;
    }

    json jobj = load_json(vm["conf"].as<string>());
    BallLogLikelihood bll(jobj.at("ball_log_lh"));
    Binarizer bin(jobj.at("binarizer"));
    FindBallBlob bf(jobj.at("blob_detection"));
    int subsample = 0;
    if (jobj.count("subsample")) subsample = jobj.at("subsample");

    string fname = vm["input"].as<string>();
    VideoCapture in_video(fname);
    Size in_size((int)in_video.get(CV_CAP_PROP_FRAME_WIDTH)>>subsample, 
        (int)in_video.get(CV_CAP_PROP_FRAME_HEIGHT)>>subsample);
    int ex = static_cast<int>(in_video.get(CV_CAP_PROP_FOURCC)); // Use same input codec
    // Transform from int to char via Bitwise operators
    char codec[] = {(char)(ex & 0XFF) , 
      (char)((ex & 0XFF00) >> 8),
      (char)((ex & 0XFF0000) >> 16),
      (char)((ex & 0XFF000000) >> 24), 0};
    cout << "Input video codec: " << codec << " fps: " << in_video.get(CV_CAP_PROP_FPS) << endl;

    string outname = vm["output"].as<string>();
    Size out_size(2*in_size.width, 2*in_size.height);
    VideoWriter out_video(outname, ex,//VideoWriter::fourcc(codec[0], codec[1], codec[2], codec[3]),
        in_video.get(CV_CAP_PROP_FPS), out_size);

    if (!in_video.isOpened()) {
      cerr << "Input video could not be opened: " << fname << endl;
      return -1;
    }
    if(!out_video.isOpened()) {
      cerr << "Output video could not be opened: " << outname << endl;
      return -1;
    }

    Mat img, l_lh, tmp;
    Mat res(out_size, CV_8UC3);
    vector<double> llh_time, bin_time, blob_time, proc_time, in_time, draw_time;
    while (true) {
      //1) Read the image
      auto start_t = std::chrono::steady_clock::now();
      auto st_0 = start_t;
      in_video >> img;
      if (img.empty()) break;
      auto end_t = std::chrono::steady_clock::now();
      in_time.push_back(std::chrono::duration_cast<std::chrono::microseconds>(end_t - start_t).count()); 
      //1.1) Subsample
      for (int i=0; i<subsample; i++) {
        Mat tmp;
        pyrDown(img, tmp, Size(img.cols/2, img.rows/2));
        img = tmp;
      }

      //2) Compute log likelihood image
      start_t = std::chrono::steady_clock::now();
      l_lh = bll(img);
      end_t = std::chrono::steady_clock::now();
      llh_time.push_back(std::chrono::duration_cast<std::chrono::microseconds>(end_t - start_t).count());

      //2.1) Binarize (Just for debugging purposes)
      start_t = std::chrono::steady_clock::now();
      auto bin_img = bin(l_lh);
      end_t = std::chrono::steady_clock::now();
      bin_time.push_back(std::chrono::duration_cast<std::chrono::microseconds>(end_t - start_t).count());

      //3) Run blob detection algorithm
      start_t = std::chrono::steady_clock::now();
      auto key_pts = bf(l_lh);
      end_t = std::chrono::steady_clock::now();
      blob_time.push_back(std::chrono::duration_cast<std::chrono::microseconds>(end_t - start_t).count());
    
      proc_time.push_back(llh_time.back() + bin_time.back() + blob_time.back());
      cout << "N: " << key_pts.size() << ", FPT: " << proc_time.back() << " us, ";
      cout << "llh: " << llh_time.back() << " us, bin: " << bin_time.back() << " us, blob: " << blob_time.back() << " us, ";

      //4) Compute output image
      start_t = std::chrono::steady_clock::now();
      drawKeypoints(img, key_pts, res(Rect(0,0,in_size.width,in_size.height)),
          Scalar(255,255,0), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
      normalize(l_lh, tmp, 0, 1, NORM_MINMAX);
      tmp.convertTo(tmp, CV_8U, 255);
      drawKeypoints(tmp, key_pts, res(Rect(in_size.width,0,in_size.width,in_size.height)), 
          Scalar(255,255,0), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
      //res.adjustROI(0,0,in_size.width,0);
      drawKeypoints(bin_img, key_pts, res(Rect(0,in_size.height,in_size.width,in_size.height)), 
          Scalar(255,255,0), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
      end_t = std::chrono::steady_clock::now();
      draw_time.push_back(std::chrono::duration_cast<std::chrono::milliseconds>(end_t - start_t).count());
      cout << "Draw: " << draw_time.back() << " ms, ";
    
      //5) Write output
      start_t = std::chrono::steady_clock::now();
      out_video << res;

      //6) Display output
      imshow("Output", res);
      int k = waitKey(5) & 0xff;
      end_t = std::chrono::steady_clock::now();
      cout << " TT: " << std::chrono::duration_cast<std::chrono::milliseconds>(end_t - st_0).count() 
        << " ms" << endl;
      if (k==27) break;
    }
    cout << "llh: " << comp_stats(llh_time) << " us, bin: " << comp_stats(bin_time) << " us, blob: " << comp_stats(blob_time) << " us" << endl;
    return 0;
  } catch (std::exception& ex) {
    cerr << "Exception: " << ex.what() << endl;
    return 1;
  }
}
