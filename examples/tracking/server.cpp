
#include <ball_tracking/server.hpp>
#include <ball_tracking/utils.hpp>
#include <iostream>
#include <boost/program_options.hpp>
#include <json.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;
using namespace ball_tracking;
using json = nlohmann::json;

int main(int argc, char** argv) {
  try {
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()
      ("help", "Produce help message")
      ("conf,c", po::value<string>(), "path to the JSON configuration");
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
      cout << desc << endl;
      return 0;
    }
    if (!vm.count("conf")) {
      cerr << "Error: You should provide the video to process" << endl;
      return 1;
    }

    json server_conf = load_json(vm.at("conf").as<string>());
    cout << "Loading server configuration" << endl;
    TrackServer srv; 
    srv.start(server_conf);
    cout << "Tracking started. Type q to quit" << endl;
    char c;
    do {
      cin >> c;
    } while (c != 'q');
    cout << "Stopping" << endl;
    srv.stop();
  } catch (std::exception& ex) {
    cerr << "Exception: " << ex.what() << endl;
    return 1;
  }
}
