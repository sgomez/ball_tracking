find_package(Boost COMPONENTS unit_test_framework REQUIRED)
add_executable(test_img_proc
  test_img_proc.cpp
  )
target_link_libraries(test_img_proc
  ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY}
  ball_tracking
  )
