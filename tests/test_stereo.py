import numpy as np
import unittest
import zmq
import subprocess
import matplotlib.pyplot as plt
import os
import json
import time

class TestStereo(unittest.TestCase):

    def setUp(self):
        build_path = '../build/release'
        ps_path = os.path.join(build_path,'examples/track_server3d')
        conf_path = './config/server_3d_conf.json'
        cmd = [ps_path, '-c', conf_path]
        self.server_process = subprocess.Popen(cmd)
        self.conf = json.load(file(conf_path,'r'))
        ctx = zmq.Context()
        self.obs2d = ctx.socket(zmq.PUB)
        self.obs2d.bind("tcp://*:7650")
        self.obs3d = ctx.socket(zmq.SUB)
        self.obs3d.connect("tcp://localhost:7660")
        self.obs3d.setsockopt(zmq.SUBSCRIBE, "")
        time.sleep(0.5)
        print("Set up succedded")

    def tearDown(self):
        self.server_process.terminate()
        print("Sending terminate signal")
        time.sleep(0.5)

    def test_no_outliers(self):
        pts_x = np.linspace(-1,1,10)
        pts_y = np.linspace(-4,0,10)
        pts_z = np.linspace(-2,0,10)
        num = 0
        for x in pts_x:
            for y in pts_y:
                for z in pts_z:
                    for c in self.conf["stereo"]["calib"]:
                        c_id = c['ID']
                        C = np.array(c['val'])
                        pt2dh = np.dot(C, np.array([x,y,z,1.0]))
                        pt2d = [pt2dh[0]/pt2dh[2], pt2dh[1]/pt2dh[2]]
                        pt2d_msg = {"cam_id": c_id, "num": num, "obs": pt2d, "time": 0.0}
                        #print(json.dumps(pt2d_msg))
                        self.obs2d.send(json.dumps(pt2d_msg))
                    num += 1
                    pt3d_msg = self.obs3d.recv()
                    pt3d_j = json.loads(pt3d_msg)
                    self.assertLessEqual(np.linalg.norm(
                        np.array(pt3d_j['obs']) - np.array([x,y,z])), 0.02)
        #print(num)
                 

    def test_with_outliers(self):
        pass

if __name__=='__main__':
    unittest.main()
