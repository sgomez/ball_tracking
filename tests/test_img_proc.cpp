
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE BallModels
#include <boost/test/unit_test.hpp>
#include <json.hpp>

#include <opencv2/opencv.hpp>
#include <ball_tracking/img_proc.hpp>
#include <ball_tracking/utils.hpp>

using namespace ball_tracking;
using namespace std;
using namespace cv;
using json = nlohmann::json;

BOOST_AUTO_TEST_CASE( test_map_channel_quad ) {
  json jobj = load_json("tests/config/color_approach.json");
  Mat mean = json2cvmat(jobj.at("mean"));
  Mat inv_cov = json2cvmat(jobj.at("inv_cov"));

  Mat img = imread("tests/images/pic1.png", CV_LOAD_IMAGE_COLOR);
  Mat py_result = imread("tests/images/result_color.jpg", 0);
  py_result.convertTo(py_result, CV_64FC3);

  Mat im_proc = map_channel_quad(img, mean, inv_cov);
  exp(-0.5*im_proc, im_proc);
  im_proc = 255*im_proc;

  Mat diff = py_result - im_proc;
  double dif_norm = cv::norm( diff ) / sqrt(diff.rows * diff.cols);
  cout << dif_norm << endl;
  BOOST_CHECK(dif_norm < 1.0);
}
